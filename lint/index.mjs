
import fs from 'fs'
import path from 'path'

import { ESLint } from "eslint";
import BableEslint from '@babel/eslint-parser'

function find_zunzun(current_dir) {
  const possible_path = path.join(current_dir, "node_modules/zunzun");
  if (fs.existsSync(possible_path)) {
    return possible_path;
  } else if (possible_path !== "/node_modules/zunzun") {
    return find_zunzun(
      path.resolve(current_dir, "..")
    );
  } else {
    throw new Error(`No node_modules/zunzun directory found!`);
  }
}

export default class Lint {
  static async syntax_errors_module(module_dir) {
    const eslint = new ESLint({
      cwd: module_dir,
      overrideConfigFile: true,
      overrideConfig: {
        files: ["**/*.mjs"],
        ignores: ["**/dist/*.js"],
        rules: {
          "no-unused-vars": "off",
          "no-console": "off",
          "no-debugger": "off",
          "no-mixed-spaces-and-tabs": "off",
          "semi": "off",
          "quotes": "off",
          "indent": "off",
          "comma-dangle": "off"
        }
      },
      baseConfig: {
        languageOptions: {
          parser: BableEslint,
          parserOptions: {
            requireConfigFile: false
          }
        }
      }
    });
    const results = await eslint.lintFiles([module_dir+"/**/*.mjs"]);
    const formatter = await eslint.loadFormatter("stylish");
    const resultText = formatter.format(results);
    if (resultText.length > 0) console.log(resultText);
  }

  static async syntax_errors(module_dir) {
    await Lint.syntax_errors_module(module_dir);
    
    let zunzun_dir = undefined
    try {
      const zunzun_dir = find_zunzun(module_dir);
    } catch (e) {
      console.log(e.stack);
    }

    if (zunzun_dir) {
      const twig_json_path = path.resolve(module_dir, "twig.json");
      const twig_json = JSON.parse(fs.readFileSync(twig_json_path, 'utf8'));
      for (const lib in twig_json.lib) {
        const dep_dir = path.join(zunzun_dir, lib);
        await Lint.syntax_errors(dep_dir);
      }
    }

  }

}
