
import fs from 'fs'
import path from 'path'


import { fileURLToPath } from 'url';
const __dirname = path.dirname(fileURLToPath(import.meta.url));



import Lint from './lint/index.mjs'

const CWD = process.cwd();

function log_status(color_blocks) {
  let report_time = new Date();
  report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);

  let rmsg = `\x1b[36m ${report_time} \x1b[0m`;

  for (let c = 0; c < color_blocks.length; c++) {
    let cblock = color_blocks[c];
    for (let v = 0; v < cblock.length; v++) {
      let cbval = cblock[v];
      if (v == cblock.length-1) {
        rmsg += `${cbval}`;
        if (v > 0) {
          rmsg += `\x1b[0m`;
        }
      } else {
        rmsg += `\x1b[${cbval}m`;
      }
    }
  }

  console.log(rmsg);
}

export default class FlyModule {
  static async load(module_dir, module_name, nocache) {
    try {
      let cache_hack = '';
      if (nocache) {
        cache_hack = `?update=${Date.now()}`
      }
      return await import(module_dir+"/"+module_name+cache_hack);
    } catch (e) {
      /*
       * Iterate through all services that are in twigs.json dependency list and lint them as well
       * This only lints the code in yhr module_path. Resolve node_modules path by going up a directory until existing with an error.
       */
      await Lint.syntax_errors(module_dir);
      console.log(e);

      return undefined;
    }
  }

  static async run_preconfig(name, module_path, config, service_manager) {
    try {
      log_status([
        [1, 33, "PRE-CONFIG"],
        [" --"],
        [5, "-> "],
        [1, 32, 4, name]
      ]);

      let this_module = await FlyModule.load(module_path, "preconf.mjs");
      if (!this_module) return undefined;

      let resulting_config = undefined;
      if (this_module.default.run) {
        resulting_config = await (this_module.default.run(service_manager, config));
      }

      console.log("PRECONFIGURED", resulting_config)

      return resulting_config;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static async start_service(name, module_path, config, service_manager) {
    try {
      log_status([
        [1, 34, "MODULE"],
        [" --"],
        [5, "-> "],
        [1, 36, 4, name]
      ]);

      let this_module = await FlyModule.load(module_path, "index.mjs");
      if (!this_module) return undefined;

      let module_class_instance_object = undefined;

      if (this_module.default.start) {
        module_class_instance_object = await (this_module.default.start(service_manager, config));
      }

      return {
        module: this_module,
        instance_object: module_class_instance_object
      };
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }


  static async run_script(name, module_path, config, zunzun_config, project_config) {
    try {
      log_status([
        [1, 91, "SCRIPT"],
        [" --"],
        [5, "-> "],
        [1, 36, 4, name]
      ]);

      let this_module = await FlyModule.load(module_path, "index.mjs");
      if (!this_module) return undefined;

      let module_class_instance_object = undefined;

      if (this_module.default.run) {
        module_class_instance_object = await (this_module.default.run(config, zunzun_config, project_config));
      }

      return {
        module: this_module,
        instance_object: module_class_instance_object
      };
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}

